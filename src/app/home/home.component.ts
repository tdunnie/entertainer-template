import { Component, OnInit } from '@angular/core';
import { Entry } from 'contentful';
import { ContentfulService } from '../contentful/contentful.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  heroSection: Entry<any>;

  constructor(private readonly service: ContentfulService) { }

  ngOnInit() {

    this.service.getSectionContent('homePageContent').then(result => {
      this.heroSection = result[0];
      console.log(result);
    })
  }

}
