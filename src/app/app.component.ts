import { AfterViewInit, Component, ElementRef, HostListener, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'Web-Template';
  sticky = 0;

  constructor(private el: ElementRef, private router: Router) { }

  @HostListener('window:scroll', ['$event']) onWindowScroll(e) {
    const header = document.getElementById("navbar");

    if (document.scrollingElement.scrollTop > 10) {
      header.classList.add('active');
    } else {
      header.classList.remove('active');
    }

  }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });

    var element= document.getElementsByClassName('nav-link');
    for(var i=0;i<element.length;i++){
         element[i].addEventListener("click", function(){document.getElementById('navbarSupportedContent').classList.remove('show')}, false);   
    }
  }

  ngAfterViewInit(): void {
    (<any>window).twttr.widgets.load();
}


}
