import { Component, OnInit } from '@angular/core';
import { Entry } from 'contentful';
import { ContentfulService } from '../contentful/contentful.service';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss']
})
export class VideosComponent implements OnInit {
  videos: Entry<any>[];

  constructor(private readonly service: ContentfulService) { }

  ngOnInit() {

    this.service.getSectionContent('videoGallery').then(result => {
      this.videos = result[0]['fields']['videos'];
      console.log(result);
    })
  }

}
