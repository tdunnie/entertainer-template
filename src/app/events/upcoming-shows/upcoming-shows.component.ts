import { Component, OnInit } from '@angular/core';
import { Entry } from 'contentful';
import * as moment from 'moment';
import { ContentfulService } from 'src/app/contentful/contentful.service';
@Component({
  selector: 'app-upcoming-shows',
  templateUrl: './upcoming-shows.component.html',
  styleUrls: ['./upcoming-shows.component.scss']
})
export class UpcomingShowsComponent implements OnInit {

  events: Entry<any>[];

  constructor(private readonly service: ContentfulService) { }

  ngOnInit() {
    // {'fields.date[gte]': '2021-11-04T00:00:00Z'}
    this.service.getSectionContent('events', {'fields.date[gte]': moment().subtract(1, 'days').toISOString()}).then(result => {
      this.events = result;
      const a = result.sort((a, b) =>  {
         return new Date(a['fields']['date']).getTime() - new Date(b['fields']['date']).getTime();
      });
    })
  }

  parseLink(link: string): string {
    const isValid = this.validURL(link);
    return isValid? link : '#'
  }

  getLinkString(link: string): string {
    return this.validURL(link)? 'Buy Tickets' : link;
  }

  validURL(str): boolean {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
  }

  getFormatedDate (date:string): string {
    return `${moment(date).format('MMM')} ${moment.parseZone(date).format('DD')}`
  }

}
