import { Component, OnInit } from '@angular/core';
import { CalendarOptions } from '@fullcalendar/angular';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {

  Events = [{
    title: 'Test Show',
    start: '2021-09-11'
  },
  {
    title: 'Test Show 2',
    start: '2021-10-31',
    test: 'hello'
  }];
  calendarOptions: CalendarOptions;

  constructor() { }

  ngOnInit() {
    this.calendarOptions = {
      initialView: 'dayGridMonth',
      eventClick: this.onEventClick.bind(this),
      events: this.Events
    };
  }
  onEventClick (res: any): void {
      debugger;
  }
}
