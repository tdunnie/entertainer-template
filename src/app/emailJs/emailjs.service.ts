import { Injectable } from '@angular/core';
import * as emailjs from 'emailjs-com';
import { ContactUsModel } from '../contact-us/contact-us.model';

@Injectable({
  providedIn: 'root'
})
export class EmailjsService {

  constructor() { }

  sendEmail(data: ContactUsModel) {

    const emailData = {
      'from': data.from,
      'email': data.email,
      'phone': data.phone,
      'message': data.message,
      'replyTo': data.replyTo,
    };
    return emailjs.send('service_wga183j', 'template_3msatoh', emailData, 'user_XIwk1SftwlRWIGLa8lnC9')
          .then((response) => {
             return response;
          }, (err) => {
             console.log('FAILED...', err);
          });
      }
  }
