import { Component, OnInit } from '@angular/core';
import { Entry } from 'contentful';
import { ContentfulService } from '../contentful/contentful.service';
import { documentToHtmlString } from '@contentful/rich-text-html-renderer';
import { BLOCKS, INLINES} from '@contentful/rich-text-types';

@Component({
  selector: 'app-bio',
  templateUrl: './bio.component.html',
  styleUrls: ['./bio.component.scss']
})
export class BioComponent implements OnInit {
  bio: Entry<any>;
  gallery: Entry<any>;
  html: any;
  options = {
    renderNode: {
      [BLOCKS.EMBEDDED_ASSET]: (node) => `<img alt="${node.data.target.fields.title}" style="max-width: 50%; height:300px; padding: 10px;" class="img-responsive" src="https://${node.data.target.fields.file.url}"/>`,
      [INLINES.HYPERLINK]: (node) => `<a href="${node.data.uri}" target="_blank">${node.content[0].value}</a>`,
    }
  }

  constructor(private readonly service: ContentfulService) { }

  ngOnInit() {

    this.service.getSectionContent('bio').then(result => {
      this.bio = result[0];
      this.gallery = result[0]['fields']['bioGallery'];
      this.html = documentToHtmlString(this.bio.fields['bioText'], this.options);
      document.getElementById('rich-text-body1').innerHTML = this.html;
      console.log(result);
    })
  }
}

