import { Component, OnInit } from '@angular/core';
import { Entry } from 'contentful';
import { ContentfulService } from '../contentful/contentful.service';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.scss']
})
export class PhotosComponent implements OnInit {
  gallery: Entry<any>;
  title = 'photos'

  constructor(private readonly service: ContentfulService) { }

  ngOnInit() {

    this.service.getSectionContent('photoGallery').then(result => {
      this.gallery = result[0]['fields']['images'];
      console.log(result);
    })

  }
}
