import { Injectable } from '@angular/core';
import { createClient, Entry } from 'contentful';
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ContentfulService {

  private client = createClient({
    space: environment.spaceId,
    accessToken: environment.accessToken
  });

  constructor() { }

  getSectionContent(contentType: string, query?: object): Promise<Entry<any>[]> {

    return this.client.getEntries(Object.assign({
      content_type: contentType,
    }, query))
      .then(res => {
        console.log(res);
        return res.items;

      });
  }

  getEntry(id: string, query?: object): Promise<any> {

    return this.client
    .getEntry(id)
    .then(entry => {
      return entry;
    })
    .catch(error => console.log(error));
  }
}
