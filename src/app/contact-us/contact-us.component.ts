import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EmailjsService } from '../emailJs/emailjs.service';
import { ContactUsModel } from './contact-us.model';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  sending: boolean;
  emailSent: boolean;
  errorSending: boolean;
  contactUsFormGroup = new FormGroup({});

  constructor(private emailService: EmailjsService, private cd: ChangeDetectorRef) { }

  ngOnInit() {
    this.contactUsFormGroup = new FormGroup({
      from: new FormControl('', Validators.required),
      message: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      phone: new FormControl('', Validators.required)
    });
  }

  sendEmail() {
    if (!this.contactUsFormGroup.valid) {
      this.contactUsFormGroup.markAllAsTouched();
      return
    }
    this.sending = true;
    this.emailService.sendEmail(new ContactUsModel(this.contactUsFormGroup.value)).then(response =>  {
        this.emailSent = true;
        this.errorSending = false;
        this.contactUsFormGroup.reset();
        this.cd.detectChanges();
    }).catch(error => {
       this.emailSent = false;
       this.errorSending = true;
    });
    this.sending = false;
  }

}
