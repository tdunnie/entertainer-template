export class ContactUsModel {
    from: string;
    service: string;
    email: string;
    phone: string;
    message: string;
    replyTo: string;

    constructor (init?: Partial<ContactUsModel>) {
        Object.assign(this, init);
      }
}