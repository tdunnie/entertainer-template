import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

class MailChimpResponse {
  result: string;
  msg: string;
}

@Injectable({
  providedIn: 'root'
})
export class MailchimpService {

  constructor(private readonly client: HttpClient) { }

  subscribeNewMember(email: string) : Observable<MailChimpResponse> {

    const API_KEY = '50a6ec9308a793dd17d4ddcbab287a10-us1';
    const SEND_WELCOME = true;
    // subscriber details

    const URL = environment.mailchimpUrl
    const params = new HttpParams()
    .set('b_5a726cf9274591936b171836b_5f16c0c72d', '')
    .set('EMAIL', email);

    return this.client.jsonp<MailChimpResponse>(URL + params.toString(), 'c').pipe(
      tap((response: MailChimpResponse) => console.log(response))
    );
  }

}
