import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BioComponent } from './bio/bio.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { EventsComponent } from './events/events.component';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { PhotosComponent } from './photos/photos.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { VideosComponent } from './videos/videos.component';

const routes: Routes = [];

@NgModule({
  imports: [
    RouterModule.forRoot([
        { path: '', component: HomeComponent},
        { path: 'events', component: EventsComponent},
        { path: 'bio', component: BioComponent},
        { path: 'videos', component: VideosComponent},
        { path: 'photos', component: PhotosComponent},
        { path: 'book-me', component: ContactUsComponent},
        { path: 'privacy-policy', component: PrivacyPolicyComponent},
        { path: '**', component: NotFoundComponent},
        
    ])
],
  exports: [RouterModule]
})
export class AppRoutingModule { }
