import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MailchimpService } from '../mailchimp/mailchimp.service';

@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.scss']
})
export class NewsletterComponent implements OnInit {
  newsletterForm: FormGroup;
  sent: boolean;
  errorSending: boolean;

  constructor(private readonly mailchimp: MailchimpService) { }

  ngOnInit(): void {


    this.newsletterForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
    });
  }

  subscribeEmail (): void {
    this.mailchimp.subscribeNewMember(this.newsletterForm.controls.email.value).subscribe((response) => {
      this.sent = true;
      this.newsletterForm.reset();
      console.log(response);
    }, err => {
      this.errorSending = true;
      console.log(err);
    });
  }
}
